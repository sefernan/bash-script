#!/bin/bash

#___________________________________________________________________________________________________
#                       This is a bash script for matching folders of samples 
#___________________________________________________________________________________________ryse-amp

# Parse outputs & declarations

echo "Script name is: $0"
echo " "
echo "you are usign the script for link the information that comes from rucio and the local folders (CxAOD samples)"
echo " "
echo "The sample that you're running is: $1"
echo " "
echo "The folder-region is: $2"
echo " "
echo "From the target path /eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/ where all the CxAOD samples in study to check is..."
echo "This is the list of directories that the script will cover: "
echo " " 

# Create the text file with all the paths that we want to analysed with the specific sample

find /eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/ -type d | grep -i "$1" | grep -i ".root" | grep  -i "$2" | grep -v "deleted" > list_of_path_$1_$2.txt

while read line; do 

   echo $line

done < list_of_path_$1_$2.txt

echo " "
echo "The script create a text file with all the paths reviewed named: list_of_path_$1.txt"
echo " "

# Uses the new file with all the paths

cat list_of_path_$1_$2.txt | while read line1 || [[ -n $line1 ]];
do
   echo " "
   echo "The path running is: " $line1
   echo " "

   #Cut the dataset name to use in the rucio command

   echo "Retain the part of the path after the last slash is: "
   NAME=${line1##*/}  # retain the part after the last slash
   echo $NAME
   echo " "
   
   # hasta aquí

   echo "This is the retrieve information from rucio "
   echo " "
   rucio list-files $NAME | cut -b 19-61 | grep -v "SCOPE" | grep -i ".root" > NoSort_$NAME.txt
   
   while read line2; do

     echo $line2
 
   done < NoSort_$NAME.txt
   
   sort NoSort_$NAME.txt > FromRucio_$NAME.txt


   echo " "
   echo "This is the content of the folder: " 
   echo " "

   ls $line1 > FromFolder_$NAME.txt

   while read line3; do

      echo $line3
   
   done < FromFolder_$NAME.txt
   
   # Do the match between the text files
   
   echo "aqui va el match with diff: "
   echo " "
   diff -s FromRucio_$NAME.txt  FromFolder_$NAME.txt 
   diff -q FromRucio_$NAME.txt  FromFolder_$NAME.txt 
#  sort FromRucio_$NAME.txt FromFolder_$NAME.txt|uniq -u
#  diff --brief <(sort file1) <(sort file2)

done
